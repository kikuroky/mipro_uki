let myApp = angular.module('myApp', []);
myApp.controller('AppCtrl', ['$scope', '$http', function($scope, $http) {
    console.log("test controller");


var refresh = function() {
  $http.get('/emp').success(function(response) {
    console.log("requested");
    $scope.employeelist = response;
    $scope.list = "";
  });
};

refresh();

$scope.addEmp = function() {
  console.log($scope.list);
  $http.post('/emp', $scope.list).success(function(response) {
    console.log(response);
    refresh();
  });
};

$scope.delEmp = function(id) {
  console.log(id);
  $http.delete('/emp/' + id).success(function(response) {
    refresh();
  });
};

$scope.editEmp = function(id) {
  console.log(id);
  $http.get('/emp/' + id).success(function(response) {
    $scope.list = response;
  });
};  

$scope.updateEmp = function() {
  console.log($scope.list._id);
  $http.put('/emp/' + $scope.list._id, $scope.list).success(function(response) {
    refresh();
  })
};

$scope.deselect = function() {
  $scope.list = "";
}

}]);