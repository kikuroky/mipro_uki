const express = require('express')
const app = express()
const mongojs = require('mongojs')
const db = mongojs('employees', ['emp'])
const bodyParser = require('body-parser')

app.use(express.static(__dirname + '/public'))
app.use(bodyParser.json())

app.get('/emp', function(req, res) {
    console.log('requested')
    db.emp.find(function(err, docs) {
        console.log(docs)
        res.json(docs)
    })
})

app.get('/emp/:id', function(req, res) {
    let id = req.params.id
    db.emp.findOne({_id: mongojs.ObjectId(id)}, function(err, doc) {
        res.json(doc)
    })
})

app.post('/emp', function(req, res) {
    console.log(req.body)
    db.emp.insert(function(err, doc) {
        res.json(doc)
    })
})

app.put('/emp', function(req, res) {
    let id = req.params.id
    db.emp.findAndModify({
        query: {_id: mongojs.ObjectId(id)},
        update: {$set: {
            code_emp: req.body.code_emp,
            name: req.body.name,
            department: req.body.department
        }},
        new: true}, function(err, docs) {
            res.json(docs)
    })
})

app.delete('/emp/:id', function(req, res) {
    let id = req.params.id
    console.log(id)
    db.emp.remove({_id: mongojs.ObjectId(id)}, function(err, doc) {
        res.json(doc)
    })
})

app.listen(4200)
console.log("Server running on port 4200")